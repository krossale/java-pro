package cz.cvut.fel.pjv.controller;

public class MapConstants {

    // Map Constants
    public static final String MAP_PATH_NEW2 = "/maps/new2.txt";
    public static final String MAP_PATH_PJV = "/maps/pjv.txt";
    public static final String MAP_PATH_GOLD = "/maps/gold.txt";

    public static final int MAP_NEW = 0;
    public static final int MAP_PJV = 1;
    public static final int MAP_GOLD = 2;

    public static final int MAP_NEW_COL = 19;
    public static final int MAP_NEW_ROW = 6;

    public static final int MAP_PJV_COL = 9;
    public static final int MAP_PJV_ROW = 15;

    public static final int MAP_PJV_TO_GOLD_COL = 28;
    public static final int MAP_PJV_TO_GOLD_ROW = 28;

    public static final int MAP_GOLD_COL = 9 ;
    public static final int MAP_GOLD_ROW = 15 ;

    // Merchant Constants
    public static final int MERCHANT_COL = 12;
    public static final int MERCHANT_ROW = 9;

    // Move Constants
    public static final String UP = "up";
    public static final String DOWN = "down";
    public static final String LEFT = "left";
    public static final String RIGHT = "right";
}

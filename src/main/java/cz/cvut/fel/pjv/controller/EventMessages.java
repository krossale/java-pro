package cz.cvut.fel.pjv.controller;

public class EventMessages {
    public static final String DIALOGUE_TELEPORT_ISLAND = "You used teleport to Island!";
    public static final String DIALOGUE_TELEPORT_FEL = "You used teleport to FEL!";
    public static final String DIALOGUE_DAMAGE_PIT = "You fall into a pit!";
    public static final String DIALOGUE_HEALING_POOL = "You drink the Russian VODKA\nLife and mana have been recovered\n";
    public static final String DIALOGUE_SAVING = "(The progress has been saved)";
}

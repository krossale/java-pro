package cz.cvut.fel.pjv.view;

import java.awt.*;

public class UIColors {

    // Colors used for UI elements
    public static final Color BACKGROUND_GREEN = new Color(70,120,80);
    public static final Color CURSOR_COLOR = new Color(240,190,90);
    public static final Color SHADOW_COLOR = new Color(60,60,60);
    public static final Color BLACK = new Color(0,0,0, 200);
    public static final Color WHITE = new Color(255,255,255);
    public static final Color GRAY = new Color(128, 128, 128);
    public static final Color PARTICLE_COLOR_BROWN = new Color(65,50,30);
    public static final Color HP_BAR_BACKGROUND_COLOR = new Color(35, 35, 35);
    public static final Color HP_BAR_COLOR = new Color(255, 0, 30);
    public static final Color PATH_COLOR = new Color(255, 0, 0, 70);
    public static final Color PARTICLE_COLOR = new Color(240,50,0);

}

package cz.cvut.fel.pjv.view;

public class GameConstants {

    // Title
    public static final String TITLE_NAME = "TANKI 2D GAME";
    public static final String TITLE_NEW_GAME = "NEW GAME";
    public static final String TITLE_LOAD_GAME = "LOAD GAME";
    public static final String TITLE_QUIT = "QUIT";

    // Font Name
    public static final String FONT_ARIAL = "Arial";

    // Options
    public static final String OPTIONS = "Options";
    public static final String GAME_OVER = "Game Over :/";
    public static final String RETRY = "Try again";
    public static final String QUIT = "Quit";
    public static final String SELECTOR = ">";
    public static final String CONTROL = "Control";
    public static final String MOVE = "Move";
    public static final String CONFIRM = "Confirm";
    public static final String ATTACKING = "Attack";
    public static final String SHOOT = "Shoot";
    public static final String INVENTORY = "Inventory";
    public static final String PAUSE = "Pause";
    public static final String PAUSE_PRESSED = "Paused";
    public static final String GO_BACK = "Go back";
    public static final String BUY = "Buy";
    public static final String SELL = "Sell";
    public static final String LEAVE = "Leave";
    public static final String YES_OPTION = "YES";
    public static final String NO_OPTION = "NO";
    public static final String SEPARATOR = "\n";
    public static final String BACK_OPTION = "[ESC] Back";
    public static final String YOUR_COINS = "Your coins: ";
    public static final String MUSIC_VOLUME = "Music";
    public static final String SE_VOLUME = "SE volume";
    public static final String END_GAME = "End game";
    public static final String BLANK = "";
    public static final String SLASH = "/";
    public static final int MAX_COST = 999;


    // Options command
    public static final int OPTION_MUSIC = 0;
    public static final int OPTION_SOUND_EFFECTS = 1;
    public static final int OPTION_CONTROLS = 2;
    public static final int OPTION_END_GAME = 3;
    public static final int OPTION_GO_BACK = 4;
    public static final int INDENT_SIZE = 25;

    // Game controls
    public static final String WASD_BUTTONS = "WASD";
    public static final String ENTER_BUTTON = "ENTER";
    public static final String F_BUTTON = "F";
    public static final String C_BUTTON = "C";
    public static final String P_BUTTON = "P";
    public static final String R_BUTTON = "R";

    // Dialogue Messages
    public static final String NOT_ENOUGH_MONEY = "You dont have enough money";
    public static final String NO_BAG_SPACE = "You dont have place in your Bag";
    public static final String COME_AGAIN = "Come again";
    public static final String GO_TO_MENU = "Quit the game and\nreturn to main menu?";
    public static final String FORBIDE_MESSAGE = "You cannot sell equipped item!";

    // Character Attributes and Stats
    public static final String LEVEL = "Level";
    public static final String LIFE = "Life";
    public static final String MANA = "Mana";
    public static final String STRENGTH = "Strength";
    public static final String DEXTERITY = "Dexterity";
    public static final String ATTACK = "Attack";
    public static final String DEFENSE = "Defense";
    public static final String EXP = "Exp";
    public static final String NEXT_LEVEL = "Next Level";
    public static final String COIN = "Coin";
    public static final String WEAPON = "Weapon";
    public static final String HELMET = "Helmet";

    // Sound file paths
    public static final String IRISH_SOUND = "/sound/irish.wav";
    public static final String COIN_SOUND = "/sound/coin.wav";
    public static final String SUCCESS_SOUND = "/sound/success.wav";
    public static final String POWERUP_SOUND = "/sound/powerup.wav";
    public static final String UNLOCK_SOUND = "/sound/unlock.wav";
    public static final String HIT_ENEMY_SOUND = "/sound/hitenemy.wav";
    public static final String RECEIVE_DAMAGE_SOUND = "/sound/receivedamage.wav";
    public static final String LEVEL_UP_SOUND = "/sound/levelup.wav";
    public static final String CURSOR_SOUND = "/sound/cursor.wav";
    public static final String BURNING_SOUND = "/sound/burning.wav";
    public static final String CUT_TREE_SOUND = "/sound/cuttree.wav";
    public static final String GAME_OVER_SOUND = "/sound/gameover.wav";
    public static final String STAIRS_SOUND = "/sound/stairs.wav";
    public static final String PRIZE_SOUND = "/sound/prize.wav";

    // Constants for volume levels
    public static final float MUTE = -80;
    public static final float FIRST_LEVEL = -20;
    public static final float SECOND_LEVEL = -12;
    public static final float THIRD_LEVEL = -5;
    public static final float FOURTH_LEVEL = -1;
    public static final float MAX_LEVEL = 6;

    // Sound indices
    public static final int SOUND_ZERO = 0;
    public static final int SOUND_ONE = 1;
    public static final int SOUND_TWO = 2;
    public static final int SOUND_THREE = 3;
    static final int SOUND_FOUR = 4;
    public static final int SOUND_FIVE = 5;
    public static final int SOUND_SIX = 6;
    public static final int SOUND_SEVEN = 7;
    public static final int SOUND_EIGHT = 8;
    public static final int SOUND_NINE = 9;
    public static final int SOUND_TEN = 10;
    public static final int SOUND_ELEVEN = 11;
    public static final int SOUND_TWELVE = 12 ;
    public static final int SOUND_THIRTEEN = 13 ;

}
